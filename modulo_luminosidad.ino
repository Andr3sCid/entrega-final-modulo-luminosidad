#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <espnow.h>

#define WIFI_AP "Redmi 10A"
#define WIFI_PASSWORD "Sopaipillas con mostaza"
#define TOKEN "hd6HdmYRwUVz5VUtY8Wt"
#define RELAY_PIN D3

const char thingsboardServer[] = "iot.ceisufro.cl";
const int PORT = 1883;

WiFiClient wifiClient;
PubSubClient client(wifiClient);

struct RelayStatus {
  int encendido;
  int luminosity;
  int movimiento;
};
RelayStatus relayData;

unsigned long lastSend = 0;
int relayStatus = 0;

void setup() {
  Serial.begin(115200);
  pinMode(RELAY_PIN, OUTPUT);

  InitWiFi();
  setupMQTT();
  initializeRelay();
  setupESPNow();
}

void loop() {
  maintainMQTTConnection();
  client.loop();
}

void OnRecv(uint8_t *mac, uint8_t *incomingData, uint8_t len) {
  memcpy(&relayData, incomingData, sizeof(relayData));
  Serial.print("Estado recibido: ");
  Serial.println(relayData.encendido);
  Serial.println("----------");
}

void setupESPNow() {
  if (esp_now_init() == 0) {
    esp_now_set_self_role(ESP_NOW_ROLE_SLAVE);
    esp_now_register_recv_cb(OnRecv);
  } else {
    Serial.println("Error initializing ESP-NOW");
  }
}

void setupMQTT() {
  client.setServer(thingsboardServer, PORT);
}

void initializeRelay() {
  digitalWrite(RELAY_PIN, relayStatus == 0 ? HIGH : LOW);
  delay(1000);
}

void turnOnRelay() {
  digitalWrite(RELAY_PIN, LOW);
  Serial.println("Relay ON");
}

void turnOffRelay() {
  digitalWrite(RELAY_PIN, HIGH);
  Serial.println("Relay OFF");
}

void changeRelayStatus(int newStatusRelay) {
  if (newStatusRelay == 1) {
    turnOnRelay();
  } else {
    turnOffRelay();
  }
  relayStatus = newStatusRelay;
  Serial.println("Estado actual: " + String(relayStatus));
  Serial.println("----------");
}

void maintainMQTTConnection() {
  if (!client.connected()) {
    reconnectMQTT();
  }

  if (millis() - lastSend > 1000) {
    changeRelayStatus(relayData.encendido);
    sendData();
    lastSend = millis();
  }
}

void sendData() {
  String payload = "{\"encendido\":" + String(relayData.encendido) + "}";
  char attributes[100];
  payload.toCharArray(attributes, sizeof(attributes));
  client.publish("v1/devices/me/telemetry", attributes);
  Serial.println("Payload enviado a ThingsBoard:");
  Serial.println(payload);
  Serial.println("---------------------------------------");
  payload = "{\"luminocidad\":" + String(relayData.luminosity) +"}";
  payload.toCharArray(attributes, sizeof(attributes));
  client.publish("v1/devices/me/telemetry", attributes);
  Serial.println("Payload enviado a ThingsBoard:");
  Serial.println(payload);
  Serial.println("---------------------------------------");
  payload = "{\"movimiento\":" + String(relayData.movimiento) +"}";
  payload.toCharArray(attributes, sizeof(attributes));
  client.publish("v1/devices/me/telemetry", attributes);
  Serial.println("Payload enviado a ThingsBoard:");
  Serial.println(payload);
  Serial.println("---------------------------------------");
}

void InitWiFi() {
  Serial.print("Conectando a la red Wi-Fi...");
  WiFi.begin(WIFI_AP, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Conectado a la red Wi-Fi");
  Serial.print("Dirección IP: ");
  Serial.println(WiFi.localIP());
  Serial.print("Dirección MAC: ");
  Serial.println(WiFi.macAddress());
}

void reconnectMQTT() {
  while (!client.connected()) {
    if (WiFi.status() != WL_CONNECTED) {
      WiFi.begin(WIFI_AP, WIFI_PASSWORD);
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
      }
      Serial.println("Conectado a la red Wi-Fi");
    }
    Serial.print("Conectando a ThingsBoard...");
    if (client.connect("Rele", TOKEN, NULL)) {
      Serial.println("[DONE]");
    } else {
      Serial.print("[FAILED] [ rc = ");
      Serial.print(client.state());
      Serial.println(" : reintentando en 5 segundos]");
      delay(5000);
    }
  }
}

